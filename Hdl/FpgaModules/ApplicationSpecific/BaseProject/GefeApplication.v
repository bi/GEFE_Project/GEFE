//============================================================================================\\
//##################################   Module Information   ##################################\\
//============================================================================================\\
//                                                                                         
// Company: CERN (BE-BI) 
//                                                        
// File Name: GefeApplication.v  
//
// File versions history:
//
//       DATE          VERSION      AUTHOR             DESCRIPTION
//     - 06/11/2016    1.6          M. Barros Marin    Cosmetic modifications         
//     - 03/03/2016    1.5          M. Barros Marin    Cosmetic modifications         
//     - 06/10/2015    1.0          M. Barros Marin    First .v module definition
//
// Language: Verilog 2005                                                              
//                                                                                                   
// Targeted device:
//
//     - Vendor: Microsemi 
//     - Model:  ProASIC3E(A3PE3000)/ProASIC3L(A3PE3000L) - 896 FBGA
//
// Description:
//
//     Generic HDL "application" module for the GBT-based Expandable Front-End (GEFE),
//     the standard rad-hard digital board for CERN BE-BI applications.                                                                                              
//                                                                                                   
//============================================================================================\\
//############################################################################################\\
//============================================================================================\\

`timescale 1ns/100ps

module GefeApplication    
//========================================  I/O ports  =======================================\\    
(
    //==== Resets scheme ====\\

    // Opto Loss Of Signal (LOS) reset:
    // Comment: This reset is asserted when there is not enough optical signal in the
    //          receiver of the optical transceiver (either VTRx or SFP+).
    input          OptoLosReset_iran,

    //==== GBTx ====\\
    
    // Clocks:
    // Comments: - GbtxElinksDclkCg is the clock used by the GBTx I/O registers.
    //             GbtxElinksDclkCg is connected to the "Chip global" (Cg) clock network.
    //             GbtxElinksDclkQg[0:1] are connected to "Quadrant global" (Qg) clock networks. 
    //           - In GEFE, the SC Elink uses the same reference clock as the normal Elinks (GbtxElinksDclkCg).
    //             GbtxElinksScClkQg is connected to the 1st "Quadrant global" (Qg) clock network. 
    //           - GbtxClockDesCg is connected to the "Chip global" (Cg) clock network.
    //             GbtxClockDesQg[0:2] are connected to "Quadrant global" (Qg) clock networks.
    //             GbtxClockDes[4] goes directly to the FMC connector bypassing the ProAsic3 FPGA.
    input  [ 0: 1] GbtxElinksDclkQg_ikb2p, 
    input  [ 0: 1] GbtxElinksDclkQg_ikb2n,
    input          GbtxElinksScClkQg_ikp,  
    input          GbtxElinksScClkQg_ikn,  
    input          GbtxClockDesCg_ikp,   
    input          GbtxClockDesCg_ikn,   
    input  [ 0: 2] GbtxClockDesQg_ikb3p,   
    input  [ 0: 2] GbtxClockDesQg_ikb3n,
    
    // I2C:
    inout          GbtxI2cSda_io,
    inout          GbtxI2cScl_io,

    // Control:
    output         GbtxReset_or,
    input          GbtxRxDataValid_i,
    output         GbtxTxDataValid_o,
    input          GbtxRxRdy_i,
    input          GbtxTxRdy_i,

    //==== FMC connector ====\\

    // Comment: All clocks as well as the LA, HA and DP pins are powered by Vadj, 
    //          whilst the HB pins are powered by VioBM2c.     

    // Clocks:
    // Comments: - FmcClkM2c0Cg    is connected to the "Chip global" (Cg) clock network.
    //             FmcClkM2c1Qg    is connected to the 2th "Quadrant global" (Qg) clock network.  
    //           - FmcClkBidir2Cq  is connected to the "Chip global" (Cg) clock network.
    //             FmcClkBidir3Qg  is connected to the 1st "Quadrant global" (Qg) clock network.  
    //           - FmcGbtClkM2c0Qg is connected to the 1th "Quadrant global" (Qg) clock network.  
    //             FmcGbtClkM2c1Qg is connected to the 3th "Quadrant global" (Qg) clock network.  
    input          FmcClkM2c0Cg_ikp,  
    input          FmcClkM2c0Cg_ikn,   
    input          FmcClkM2c1Qg_ikp,   
    input          FmcClkM2c1Qg_ikn,  
    inout          FmcClkBidir2Cq_iokp,
    inout          FmcClkBidir2Cq_iokn,
    inout          FmcClkBidir3Qg_iokp,
    inout          FmcClkBidir3Qg_iokn,
    input          FmcGbtClkM2c0Qg_ikp,
    input          FmcGbtClkM2c0Qg_ikn,
    input          FmcGbtClkM2c1Qg_ikp,
    input          FmcGbtClkM2c1Qg_ikn,
    
    // LA pins:
    // Comment: Please note that the following pins are Clock Capable (CC): 0, 1, 17.
    inout  [33: 0] FmcLa_iob34p,
    inout  [33: 0] FmcLa_iob34n,

    // HA pins:
    // Comment: Please note that the following pins are Clock Capable (CC): 0, 1, 17.
    inout  [23: 0] FmcHa_iob24p,
    inout  [23: 0] FmcHa_iob24n,

    // HB pins:
    // Comments: - Please note that the following pins are Clock Capable (CC): 0, 6, 17.
    //           - Referenced voltage levels may only be used by HB pins.
    inout  [21: 0] FmcHb_iob22p,
    inout  [21: 0] FmcHb_iob22n,

    // DP lanes:
    // Comment: The high-speed (DP) lanes do not complain the FMC standard (VITA 57.1)
    //          since they are used as standard IOs and some of them are also used for
    //          special purposes.
    inout  [ 0: 9] FmcDpM2c_iob10p,
    inout  [ 0: 9] FmcDpM2c_iob10n,
    inout  [ 0: 9] FmcDpC2m_iob10p,
    inout  [ 0: 9] FmcDpC2m_iob10n,

    // I2C:
    inout          FmcSda_io,
    inout          FmcScl_io,

    // JTAG:
    output         FmcTck_o,
    inout          FmcTdi_i,
    output         FmcTdo_o,
    output         FmcTms_o,
    output         FmcTrstL_on,    

    // Control:
    input          FmcClkDir_i, 
    input          FmcPowerGoodM2c_i,    
    output         FmcPowerGoodC2m_o,
    input          FmcPrsntM2cL_in,  

    //==== Miscellaneous ====\\

    // MMCX Clocks & GPIOs:
    // Comments: - MmcxClkIoCg is connected to the "Chip global" (Cg) clock network.
    //           - MmcxGpIo[3:0] are connected to different "Quadrant global" (Qg) clock networks.
    inout          MmcxClkIoCg_iokp, 
    inout          MmcxClkIoCg_iokn,
    inout  [ 0: 3] MmcxGpIoQg_iokb4,   

    // LEMO GPIO:
    // Comment: LemoGpio is connected to the 4th quadrant clock network.  
    output         LemoGpioDir_o,
    inout          LemoGpioQg_iok,   

    // Push button:
    input          PushButton_i,

    // DIP switch:
    input  [ 7: 0] DipSwitch_ib8,

    // User LEDs:
    output [ 0: 5] Leds_onb6,

    // GPIO connectors:
    inout  [12: 0] GpioConnA_iob13,
    inout  [23: 0] GpioConnB_iob24,

    // Board ID connector:
    inout  [12: 0] BoardIdConn_iob13,

    // GEFE configuration ID:
    input  [ 9: 0] GefeConfigId_ib10,

    // Electrical serial link:
    input          ElectSerialLinkRx_i,
    output         ElectSerialLinkTx_o,

    //==== Powering ====\\

    input          V1p5PowerGood_i,
    input          V2p5PowerGood_i,
    input          V3p3PowerGood_i,    
    output         V3p3Inhibit_o,
    input          V3p3OverCurMon_i,

    //==== System module interface ====\\

    // Resets Scheme:
    // Comment: See Microsemi application note AC380.
    input          GeneralReset_iran,

    // GBTx:
    // Comments: - GbtxElinksDclkCg is the clock used by the GBTx I/O registers.
    //             GbtxElinksDclkCg is connected to the "Chip global" (Cg) clock network.
    input          GbtxElinksDclkCg_ik,
    //--
    input  [79: 0] DataFromGbtx_ib80,  
    output [79: 0] DataToGbtx_ob80,       
    input  [ 1: 0] DataFromGbtxSc_ib2,
    output [ 1: 0] DataToGbtxSc_ob2,    

    // Miscellaneous:
    // Comment: - Osc25MhzCg is connected to the "Chip global" (Cg) clock network.
    //          - ClkFeedbackICg is connected to the "Chip global" (Cg) clock network.
    input          Osc25MhzCg_ik,
    input          ClkFeedbackICg_ik,
    output         ClkFeedbackO_ok
);

//======================================  Declarations  ======================================\\

//==== Wires & Regs ====\\

// Resets scheme:
wire           AppReset_ra;
// GBTx Elinks:
reg    [79: 0] DataGbtxElinks_qb80;    
reg    [ 1: 0] DataGbtxElinksSc_qb2;

//=======================================  User Logic  =======================================\\     

//==== Resets scheme ====\\

assign AppReset_ra = ~GeneralReset_iran;

//==== GBTx ====\\

// Elinks:
// Comment: GbtxElinksDclkCg is the clock used by the GBTx I/O registers.
always @(posedge GbtxElinksDclkCg_ik)
    if (AppReset_ra) begin
        DataGbtxElinks_qb80  <= 80'b0;
        DataGbtxElinksSc_qb2 <=  2'b0;
    end else begin
        DataGbtxElinks_qb80  <= DataFromGbtx_ib80;
        DataGbtxElinksSc_qb2 <= DataFromGbtxSc_ib2;
    end

assign DataToGbtx_ob80   = DataGbtxElinks_qb80;
assign DataToGbtxSc_ob2  = DataGbtxElinksSc_qb2;

// GBTx Control:
assign GbtxReset_or      =  1'b0;
assign GbtxTxDataValid_o =  DipSwitch_ib8[0];  
assign Leds_onb6[5]      = ~GbtxRxDataValid_i;

//==== Miscellaneous ====\\

// Clock forwarding:
assign ClkFeedbackO_ok = Osc25MhzCg_ik;

// Heartbeats:
HeartBeat #(
    .g_ClkFreq   (32'd25000000))
i_Osc25MhzHb (
    .Reset_ir    (AppReset_ra), 
    .Clock_ik    (ClkFeedbackICg_ik),
    .HeartBeat_o (Leds_onb6[0]));

HeartBeat #(
    .g_ClkFreq   (32'd40000000))
i_DclkHb (
    .Reset_ir    (AppReset_ra), 
    .Clock_ik    (GbtxElinksDclkCg_ik),
    .HeartBeat_o (Leds_onb6[3]));     

// Opto Loss Of Signal (LOS) reset:
// Comment: This reset is asserted when there is not enough optical signal in the
//          receiver of the optical transceiver (either VTRx or SFP+).
assign Leds_onb6[4] = OptoLosReset_iran;

// Push button:
assign Leds_onb6[1] = ~PushButton_i;

//==== Powering ====\\

assign Leds_onb6[2] = ~(V1p5PowerGood_i & V2p5PowerGood_i & V3p3PowerGood_i);

endmodule